export const environment = {
  production: true,
  api: 'https://miniburn-api.herokuapp.com/api/',
};
